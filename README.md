# Todo Apps Mobile Apps 
 Todo Apps is task management app
 You can visit at https://ga-todo-apps.herokuapp.com/

## Description
This Apps Build based on  React.JS, The Libaries what I use is Ant Design, SASS for styling and make components.

## Usage
**How to use this App Project ?**
1.  First you must clone this repository ```git clone https://gitlab.com/ditcu-code/ga-todoapps.git ```

2.  After clone the repository  use ```cd GA-ToDoApps``` to change directory projects

3.  Use ```yarn install``` or ```npm install``` for installing all dependencies on this project

4.  After that , you also need ```yarn dev``` if you using yarn or if you using npm you can use ```npm dev``` 

5.  And if you want to run the test just use ```yarn test``` and it will be run the test that i created before

6.  Happy to run this project into your machine 😃 😃 😃 😃.........
