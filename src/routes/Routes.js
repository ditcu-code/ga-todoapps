import React, { Fragment } from "react";
import { Route } from "react-router-dom";
import HomePage from "../pages/HomePage";
import Login from "../pages/LoginPage";
import Register from "../pages/RegisterPage";
import TaskPage from "../pages/TaskPage";

const Routes = () => {
  return(
    <Fragment>
      <Route path="/" exact component={HomePage}/>
      <Route path="/login" exact component={Login}/>
      <Route path="/register" exact component={Register}/>
      <Route path="/task" exact component={TaskPage}/>
    </Fragment>
  )
}

export default Routes;
