import React, { Component } from 'react'
import { Link } from "react-router-dom";
import {Button} from "antd";
import "../styles/header.scss"

export default class Header extends Component {
    render() {
        return (
            <div>
                <div class="nav">
                    <div className="nav-logo"><Link to="/" ><p>To.dos</p></Link></div>
                    <div class="nav-list">
                        <ul>
                            <li>
                                <Link to="/login">
                                    <Button type="secondary" shape="round" onClick={<Link to="/login"></Link>} >LOGIN</Button>
                                </Link>
                            </li>
                        </ul>
                    </div>
                </div>                
            </div>
        )
    }
}
