import React, {useState, useEffect} from 'react';
import axios from 'axios';
import {useHistory} from "react-router-dom";
import {Button} from "antd";
import Img from "../../images/register.svg"
import '../../styles/loginRegister.scss';

export default function RightSideRegister () {
    const history = useHistory();
    const user = JSON.parse(localStorage.getItem('token'));

    useEffect(() => {
        if(user){
            history.push('/task')
        }
    })

    const [input, setInput] = useState(
        {
            name: '',
            email: '',
            password: '',
        }
    )

    const handleInput = e => {
        setInput({
            ...input,
            [e.target.name]: e.target.value
        })
    }
    
    const baseUrl = 'https://ga-todoapps.herokuapp.com/api/v1';
    const handleRegister = () => {
        axios.post(`${baseUrl}/user/register`, input)
            .then(res => {
                console.log("token", res)
                localStorage.setItem('token', res.data.token)
                history.push("/task");
            })
            .catch( error =>
                console.log(error)
            )
    }
    return (
        <div className="container-right">
            <div className="header">Create Account</div>
            <div className="content">
                <div className="image">
                    <img src={Img} alt="" />
                </div>
                <div className="form">
                    <div className="form-group">
                        <input type="text" name="name" placeholder="username" onChange={handleInput}/>
                    </div>
                    <div className="form-group">
                        <input type="text" name="email" placeholder="email" onChange={handleInput}/>
                    </div>
                    <div className="form-group">
                        <input type="password" name="password" placeholder="password" onChange={handleInput}/>
                    </div>
                </div>
            </div>
            <div className="footer">
                <Button size="large" type="primary" shape="round" onClick={() => handleRegister()}>SIGN UP</Button>
            </div>
        </div>
    )
}