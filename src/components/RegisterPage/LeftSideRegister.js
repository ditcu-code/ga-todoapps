import React from 'react'
import {Button} from "antd";
import { Link } from "react-router-dom";
import '../../styles/loginRegister.scss';

export default function LeftSideRegister() {
    return (
        <div className="container-left">
            <div className="logo"><Link to="/" ><p>To.dos</p></Link></div>
            <div className="content">
                <div className="title">Welcome Back!</div>
                <div className="text">To keep connected with us please login with your personal info</div>
            </div>
            <div className="footer">
                <Link to="/login">
                    <Button size="large" type="secondary" shape="round" onClick={<Link to="/register"></Link>} >LOGIN</Button>
                </Link>
            </div>
        </div>
    )
}
