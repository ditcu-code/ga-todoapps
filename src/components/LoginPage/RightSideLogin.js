import React, {useState, useEffect} from 'react';
import axios from 'axios';
import {useHistory} from "react-router-dom";
import {Button} from "antd";
import loginImg from "../../images/login.svg"
import { useSelector, useDispatch } from "react-redux";

export default function RightSideLogin () {
    const dispatch = useDispatch()
    const history = useHistory();
    const user = JSON.parse(localStorage.getItem('token'));

    useEffect(() => {
        if(user){
            history.push('/task')
        }
    })

    const [input, setInput] = useState(
        {
            name: '',
            email: '',
        }
    )

    const handleInput = e => {
        setInput({
            ...input,
            [e.target.name]: e.target.value
        })
    }
    
    const baseUrl = 'https://ga-todoapps.herokuapp.com/api/v1';
    const handleLogin = () => {
        axios.post(`${baseUrl}/user/login`, input)
            .then(res => {
                localStorage.setItem('token', res.data.token)
                history.push("/task");
            })
            .catch( error =>
                console.log(error)
            )
    }

    return (
        <div className="container-right">
            <div className="header">Login to Task Manager</div>
            <div className="content">
                <div className="image">
                    <img src={loginImg} alt="" />
                </div>
                <div className="form">
                    <div className="form-group">
                        <input type="text" name="email" placeholder="email" onChange={handleInput}/>
                    </div>
                    <div className="form-group">
                        <input type="password" name="password" placeholder="password" onChange={handleInput}/>
                    </div>
                </div>
            </div>
            <div className="footer">
            <Button size="large" type="primary" shape="round" onClick={() => handleLogin()}>LOGIN</Button>
            </div>
        </div>
    )
}
