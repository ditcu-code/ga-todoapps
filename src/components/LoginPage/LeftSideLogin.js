import React from 'react'
import {Button} from "antd";
import { Link } from "react-router-dom";

export default function LeftSideLogin() {
    return (
        <div className="container-left">
            <div className="logo"><Link to="/" ><p>To.dos</p></Link></div>
            <div className="content">
                <div className="title">Hello, Friend!</div>
                <div className="text">Enter your personal details and start your journey with us</div>
            </div>
            <div className="footer">
                <Link to="/register">
                    <Button size="large" type="secondary" shape="round" onClick={<Link to="/register"></Link>} >SIGN UP</Button>
                </Link>
            </div>
        </div>
    )
}
