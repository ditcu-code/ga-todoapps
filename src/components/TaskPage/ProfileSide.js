import React, { Component } from 'react'
import {Button} from "antd";
import axios from "axios";
// import {Modal} from "antd";
let token = localStorage.getItem("token");

export default class ProfileSide extends Component {
        state = {
            editusername: '',
            editimage: '',
            visible: false
        }

        handleChange = e => {
            this.setState({
              [e.target.name] : e.target.value
            })
          }     

        //////modal//////top
        showModal = () => {
            this.setState({
            visible: true,
            });
        };

        handleOk = async e => {
            console.log(e);
            await this.handleEditProfile();
            this.props.updateProfile();
            this.setState({
            visible: false,
            });
        };
        
        handleCancel = e => {
            console.log(e);
            this.setState({
            visible: false,
            });
        };
        //////modal//////bot

        handleEditProfile = async () => {
            const newname = this.state.editusername;
            const newimage = this.state.editimage;
            console.log("handleEditProfile", newname, newimage);
            try {
                const res = await axios.put(
                    `https://ga-todoapps.herokuapp.com/api/v1/user/profile`,
                    {
                        name: newname,
                        image: newimage
                    },
                    {
                        headers: {
                            auth: token
                        }
                    }
                );
            console.log("handleProfile", res.data)
            } catch (error) {
            console.log(error);
            }
        };

        render() {
        return (
            <div className="container-profile">
                <div className="profile">
                    <div className="profile-picture">
                        <img src={this.props.image} alt="prof pic" />
                    </div>
                    <div className="profile-name">
                        <h2><span>Hello, </span>{this.props.username} </h2>
                    </div>
                </div>
                <div className="footer">
                    {/* <Button block onClick={() => this.showModal()} >Edit Profile</Button>
                    <Modal title="Edit Task" visible={this.state.visible} onOk={this.handleOk} onCancel={this.handleCancel}>
                        <form className="modal-add" >
                          <label className="label-task" >Task Name</label><input className="modal-task" type="text" name="editusername" placeholder="loading..." defaultValue={this.props.username} onChange={this.handleChange} /> <br/>
                          <label className="label-task" >Description</label><input className="modal-task" type="file" name="editimage" placeholder="loading..." onChange={this.handleChange} />
                        </form>
                    </Modal> */}
                    &nbsp;                            
                    <Button block onClick={() => this.props.getAll()} >Show All Task</Button>
                    &nbsp;
                    <Button block onClick={() => this.props.importantTask()} >Important Task</Button>
                    &nbsp;                            
                    <Button block onClick={() => this.props.completedTask()} >Completed Task</Button>
                </div>
            </div>
        )
    }
}
