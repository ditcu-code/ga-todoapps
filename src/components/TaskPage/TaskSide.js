import React, { Component } from 'react'
import {Checkbox, Switch, Modal} from "antd";
import axios from "axios";
import { EditOutlined, DeleteOutlined } from '@ant-design/icons';
const baseUrl = "https://ga-todoapps.herokuapp.com/api/v1"
let token = localStorage.getItem("token");

export default class TaskSide extends Component {
    state = {
        name: "",
        description: "",
        due_date: "",
        isLoading: false,
        visible: false,
        editid: '',
        editname: '',
        editdescription: ''
      }

      handleChange = e => {
        this.setState({
          [e.target.name] : e.target.value
        })
      } 

      handleChangeId = e => {
        console.log("changeid")
        this.setState({
          [e.target.id] : e.target.value
        })
      } 

      //////modal//////top
      showModal = () => {
        this.setState({
          visible: true,
        });
      };

      handleOk = e => {
        console.log(e);
        this.editSubmit();
        this.setState({
          visible: false,
        });
      };
    
      handleCancel = e => {
        console.log(e);
        this.setState({
          visible: false,
        });
      };

      //////modal//////bot
      addNewTask = async(e) => {
        this.setState({ isLoading: true })
        let token = localStorage.getItem("token")
        e.preventDefault()
        const newTodo = {
          name: this.state.name,
          description: this.state.description,
          due_date: this.state.due_date
        }
        try {
          const res = await axios.post(`${baseUrl}/task/create`, newTodo, {
            headers: {
              auth: token
            }
          })
          console.log("addNewTask", res)
          this.setState({ name: "", description: "", isLoading: false })
          this.props.getAll()
        }catch(error){
          console.log(error)
        }
      }

      editSubmit = async (id) => {
        const ide = this.props.editid
        const name = this.state.editname
        const desc = this.state.editdescription
        console.log("editsubmit",ide,name,desc)
        try {
          const res = await axios.put(
            `${baseUrl}/task/${ide}`,
            {
              name: name,
              description: desc,
            },
            {
              headers: {
                auth: token,
              },
            }
          );
          console.log("handleEdit", res.data)
          this.props.getAll()
        } catch (error) {
          console.log(error);
        }
      };

    render() {
        const input = <p>heloo</p>
        const todos = this.props.todos.reverse().map(todo =>
            <ul key={todo.id} className="tasks" >
                <li className="item" >
                    <div className="item1" ><Checkbox checked={todo.completed ? true : false} onClick={() => this.props.completion(todo.id, todo.complete)} /></div>
                    <div className={todo.completed ? "item2x" : "item2"} ><h2>{todo.name} <span> {todo.description} </span> </h2> </div>
                    <div className="item3"> {todo.due_date.slice(0,10)} </div>
                    <div className="item4" ><Switch checkedChildren="YES" unCheckedChildren="NO" size="small" checked={todo.importance ? true : false} onClick={() => this.props.importance(todo.id, todo.importance)}  /></div>
                    <div className="item5" ><EditOutlined onClick={() => this.props.handleEdit(todo.id) && this.showModal()} />
                      <Modal title="Edit Task" visible={this.state.visible} onOk={this.handleOk} onCancel={this.handleCancel}>
                        <form className="modal-add" >
                          <label className="label-task" >Task Name</label><input className="modal-task" type="text" name="editname" placeholder="loading..." defaultValue={this.props.editname} onChange={this.handleChange} /> <br/>
                          <label className="label-task" >Description</label><input className="modal-task" type="text" name="editdescription" placeholder="loading..." defaultValue={this.props.editdescription} onChange={this.handleChange} />
                        </form>
                      </Modal>
                    </div>
                    <div className="item6" ><DeleteOutlined onClick={() => this.props.delete(todo.id)} /> </div>
                </li>
            </ul>
        )
 
        return (
            <div className="container-task">
                <div className="task-add">
                    <form className="input-add" onSubmit={this.addNewTask} >
                        <input className="input-task" type="text" name="name" placeholder="add a new task" value={this.state.email} onChange={this.handleChange}/>
                        <input className="input-task" type="text" name="description" placeholder="add description" value={this.state.description} onChange={this.handleChange}/>
                        <input className="input-task" type="date" name="due_date" placeholder="add description" value={this.state.due_date} onChange={this.handleChange}/>
                        <button className="submit-task" >{this.state.isLoading ? "loading..." : "ADD"} </button>
                    </form>
                </div>
                <div className="task-list">
                    <div className="title-list" >
                        <h3 className="taskTag" >Task</h3>
                        <h3 className="dueTag" >Due date</h3>
                        <h3 className="impoTag" >Important</h3>
                    </div>
                    {todos}
                </div>
            </div>
        )
    }
}
