import React from 'react';
import { Provider } from "react-redux";
import './App.css';
import store from "./stores";
import Routes from "./routes/Routes"
import "./styles/loginRegister.scss"

function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <Routes/>
      </div>
    </Provider>
  )
}

export default App;
