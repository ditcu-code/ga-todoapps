import React, { Component } from 'react'
import '../styles/loginRegister.scss';
import LeftSideRegister from '../components/RegisterPage/LeftSideRegister';
import RightSideRegister from '../components/RegisterPage/RightSideRegister';

export default class Register extends Component {
    render() {
        return (
            <div className="doom" >
                <LeftSideRegister/>
                <RightSideRegister/>
            </div>
        )
    }
}
