import React, { Component } from 'react'
import Home from "../images/homepage.svg"
import Header from "../components/Header"
import "../styles/homePage.scss"

export default class HomePage extends Component {
    render() {
        return (
            <div>
                <Header/>
                <section className="home">
                    <div className="container">
                        <div className="row">
                            <div className="home-info">
                                <h1>Increase productivity!</h1>
                                <h2>
                                    To.dos is a task management app to help you stay organized and manage your day-to-day.
                                </h2>
                            </div>
                            <div className="home-img">
                                <div className="image" >
                                    <img
                                    src={Home}
                                    alt="home"
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        )
    }
}
