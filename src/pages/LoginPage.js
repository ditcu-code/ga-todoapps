import React, { Component } from 'react'
import '../styles/loginRegister.scss';
import LeftSideLogin from "../components/LoginPage/LeftSideLogin"
import RightSideLogin from "../components/LoginPage/RightSideLogin"

export default class Login extends Component {
    render() {
        return (
            <div className="doom" >
                <LeftSideLogin/>
                <RightSideLogin/>
            </div>
        )
    }
}
