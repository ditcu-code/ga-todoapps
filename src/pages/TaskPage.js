import React, { Component } from 'react'
import HeaderDashboard from "../components/HeaderDashboard"
import "../styles/taskPage.scss"
import ProfileSide from '../components/TaskPage/ProfileSide'
import TaskSide from '../components/TaskPage/TaskSide'
import axios from "axios";
const baseUrl = "https://ga-todoapps.herokuapp.com/api/v1"
let token = localStorage.getItem("token");

export default class TaskPage extends Component {
        state = {
            todos: [],
            id: '',
            username: '',
            image: '',
            description: '',
            showCompleted: [],
            showImportant: [],
            idx: '',
            editname: '',
            editdescription: '',
            editduedate: ''
        }

        findTodo = id => {
          const found = this.state.todos.find( todo => todo.id === id)
          return found
        }
              
        getAllTask = async () => {
          try {
            const res = await axios.get(`${baseUrl}/task`,{
                headers: {
                  auth: token
                }
              })
                this.setState({todos: res.data.Task})
                console.log("refresh tasklist", this.state.todos)
              } catch(err) {
                console.log(err)
            }
          }

        getImportantTask = async () => {
          try {
            const res = await axios.get(`${baseUrl}/task/importance`,{
                headers: {
                  auth: token
                }
              })
                this.setState({todos: res.data.Task})
                console.log("showImportantTask", res.data.Task)
              } catch(err) {
                console.log(err)
            }
          }

        getCompletedTask = async () => {
          try {
            const res = await axios.get(`${baseUrl}/task/completed`,{
                headers: {
                  auth: token
                }
              })
                this.setState({todos: res.data.Task})
                console.log("showCompletedTask", res.data.Task)
              } catch(err) {
                console.log(err)
            }
          }
  
        getUserData = async () => {
          try {
              const res = await axios.get(`${baseUrl}/user/profile`,{
                  headers: {
                      auth: token
                  }
                })
              console.log("refresh profile data", res.data)
              this.setState({username: res.data.profile.name})
              this.setState({image: res.data.profile.image})
          } catch(err) {
              console.log(err)
            }
        }
        
        deleteItem = async(id) => {
          try {
            const res = await axios.delete(`${baseUrl}/task/${id}`,{
              headers: {
                auth: token
              }
              })
              console.log("deleteItem", res)
              this.setState({todos: this.state.todos.filter(item => item.id !== id)})
              this.getAllTask()
            }catch(err) {
              console.log(err)
            }
        }
        
        toggleImportant = async(id) => {
            console.log("importanceTask")
            const data = this.findTodo(id)
            const isImportant = {
              importance: !data.importance,
            }
            try {
              const res = await axios.put(`${baseUrl}/task/${id}`, isImportant, {
                headers: {
                  auth: token
                },
                data: {
                  importance : isImportant
                }
              })
              console.log("importanceTask",res.data)
              this.getAllTask()
            }catch(err) {
              console.log(err)
            }
        }

        toggleCompleted = async(id) => {
          console.log("completedTask")
          const data = this.findTodo(id)
          const isCompleted = {
            completed: !data.completed,
          }
          try {
            const res = await axios.put(`${baseUrl}/task/${id}`, isCompleted, {
              headers: {
                auth: token
              },
              data: {
                completed : isCompleted
              }
            })
            console.log("completedTask",res.data)
            this.getAllTask()
          }catch(err) {
            console.log(err)
          }
        }

        handleEdit = async (id) => {
          const deleteTodo = this.state.todos.filter((item) => item.id !== id);
          const selectedItem = this.state.todos.find((item) => item.id === id);
          console.log("handleEdit2", selectedItem.name, selectedItem.id, selectedItem.due_date);
          try {
            const res = await axios.put(
              `${baseUrl}/task/${id}`,
              {
                name: selectedItem.name,
                description: selectedItem.description,
                id: selectedItem.id,
                due_date: selectedItem.due_date
              },
              {
                headers: {
                  auth: token
                }
              }
            );
            console.log("handleEdit", res.data)
            this.setState({
              data: deleteTodo,
              editname: selectedItem.name,
              editdescription: selectedItem.description,
              editduedate: selectedItem.due_date,
              idx: selectedItem.id
            });
          } catch (error) {
            console.log(error);
          }
        };
      
        handleEditProfile = async () => {
          console.log("handleEditProfile", this.state.image, this.state.username);
          try {
            const res = await axios.put(
              `${baseUrl}/user/profile`,
              {
                name: this.state.username,
                image: this.state.image,
              },
              {
                headers: {
                  auth: token,
                },
              }
            );
            console.log("handleProfile", res.data)
            // this.setState({
            //   data: deleteTodo,
            //   editname: selectedItem.name,
            //   editdescription: selectedItem.description,
            //   idx: selectedItem.id
            // });
          } catch (error) {
            console.log(error);
          }
        };

        logout = e => {
          localStorage.removeItem("token");
          this.props.history.push("/")
        }
        
        componentDidMount() {
          this.getAllTask()
          this.getUserData()
        }
        
        render() {
          return (
            <div>
                <HeaderDashboard logOut={this.logout} />
                <div className="task-doom" >
                    <ProfileSide
                      username={this.state.username}
                      image={this.state.image}
                      getAll={this.getAllTask}
                      importantTask={this.getImportantTask}
                      completedTask={this.getCompletedTask}
                      editProfile={this.handleEditProfile}
                      updateProfile={this.getUserData}
                    />
                    <TaskSide
                      editid={this.state.idx}
                      editname={this.state.editname}
                      editdescription={this.state.editdescription}
                      editduedate={this.state.editduedate}
                      todos={this.state.todos}
                      delete={this.deleteItem}
                      getAll={this.getAllTask}
                      importance={this.toggleImportant}
                      completion={this.toggleCompleted}
                      handleEdit={this.handleEdit}
                    />
                </div>
            </div>
        )
    }
}